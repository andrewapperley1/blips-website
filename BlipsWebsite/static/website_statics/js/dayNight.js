/**
 * Created by andrewapperley on 2014-07-19.
 */

function dayNightMode(image) {
    setRetinaImage(image);
    var imageObject = $(image);
    var date = new Date();
    if ((date.getHours() > 18 && date.getHours() <= 23) || (date.getHours() < 6 && date.getHours() >= 0)) {
        if (window.devicePixelRatio >= 1.2) {
            imageObject.attr('src', imageObject.attr('data-dayNight')+"black_image@2x.png");
        } else {
            imageObject.attr('src', imageObject.attr('data-dayNight')+"black_image.png");
        }
    }
}