/**
 * Created by andrewapperley on 2014-07-18.
 */

function setRetinaBackgrounds() {
    $('[data-type="background"]').each(function(index, object) {
        setRetinaImage(object);
    });
}

function setRetinaImage(object) {
    if (window.devicePixelRatio >= 1.2) {
        if ($(object).attr('data-2x') != null) {
            if (object.tagName == "IMG") {
                $(object).attr('src', $(object).attr('src'));
                var width = $(object).width()+"px";
                var height = $(object).height()+"px";
                $(object).css({backgroundSize : width+" "+height, "width": width, "height": height});

            } else {
                $(object).css('background-image', "url("+$(object).attr('data-2x')+")", 'background-size', 'contain');
            }
        }
    }
}